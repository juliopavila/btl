import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Pages
import { LoginPage } from './views/login/login.page';
import { RegisterPage } from './views/register/register.page';
//Services
import { SessionService } from './session.service';


@NgModule({
    declarations: [
        LoginPage,
        RegisterPage
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { 
                path: '', 
                component: LoginPage 
            },
            {
                path: 'signup',
                component: RegisterPage
            }
        ])
    ],
    providers: [SessionService]
})
export class SessionModule { }
