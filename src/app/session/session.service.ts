import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

//enviroment
import { environment } from './../../environments/environment';

/**
 * JSON Object with headers
 */
const httpHeaders = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=utf-8'
  })
};


@Injectable({
  providedIn: 'root'
})

export class SessionService {

  //Static data 
  baseUrl: string = environment.baseUrl

  constructor(
    public http: HttpClient,
    private router: Router
  ) { }

  /**
   * Method for redirect to another page.
   * @param url String
   * @returns void.
   */
  redirect(url: string): void {
    this.router.navigateByUrl(url);
  }

  /**
   * Method for send SignUp petition to create user
   * @param body JSON Object 
   */
  signUp(body: Object): Observable<any> {
    const url = `${this.baseUrl}/session`;
    return this.http.post(url,body, httpHeaders).pipe();
  }

  /**
   * Method for send login petition
   * @param body JSON Object 
   */
  login(body: Object): Observable<any> {
    const url = `${this.baseUrl}/session`;
    return this.http.put(url,body, httpHeaders).pipe();
  }
}
