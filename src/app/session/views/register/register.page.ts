import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { SessionService } from '../../session.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage {

  signupForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private service: SessionService
  ) {
    this.signupForm = this.formBuilder.group({
      name: new FormControl(null, [Validators.required, Validators.pattern(/[A-Za-z]+/)]),
      lastname: new FormControl(null, [Validators.required, Validators.pattern(/[A-Za-z]+/)]),
      apiKey: new FormControl(),
      apiSecret: new FormControl(),
      email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
      password: new FormControl(null, [Validators.required]),
      confPass: new FormControl(null, [Validators.required]),
    },
      this.passwordMatchValidator
    )
  }

  /**
   * Method for validate password
  */
  passwordMatchValidator = (signupForm: FormGroup) => {
    return signupForm.get("password").value === signupForm.get("confPass").value ? null : { mismatch: true };
  };

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }

  /**
  * Method for call sign up service.
  * @returns void.
  */
  register(): void {
    this.loading();
    if (this.signupForm.valid) {
      this.service.signUp(this.signupForm.value).subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 201) this.alert('Confirmación', 'Se ha creado satisfactoriamente el usuario.', 201);
        else this.alert('Error', 'ha ocurrido un error, intente de nuevo.', res.status);
      })
    }
  }

  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      duration: 5000,
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string, status: number): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if (status === 201) this.goto('/');
          }
        }
      ]
    });
    await alert.present();
  }
}
