import { Component } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
//Service
import { SessionService } from './../../session.service';
import { LoadingController, AlertController } from '@ionic/angular';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  loginForm: FormGroup;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private service: SessionService,
    private userSessionService: UserSessionService
  ) {
    this.loginForm = this.formBuilder.group({
      username: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
      password: new FormControl(null, [Validators.required]),
    })
  }

  /**
   * Method for call redirect service.
   * @param url String
   */
  goto(url: string): void {
    this.service.redirect(url);
  }

  /**
   * Method for call login service.
   * @returns void.
   */
  login(): void {
    if (this.loginForm.valid) {
      this.service.login(this.loginForm.value).subscribe(res => {
        console.log(res);
        if(res.status === 200) {
          this.userSessionService.setSession(res);
          this.service.redirect('/tabs/shared');
        }
        else this.alert('Error', 'Correo o contraseña incorrecta.');
      })
    }
  }

  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }

}
