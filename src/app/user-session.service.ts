import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserSessionService {

  public session = {
    status: false,
    user: {
      name: null,
      lastname: null,
      email: null,
      apiKey: '',
      apiSecret: '',
      mount: '',
      token: null,
    }
  }
  private session$ = new Subject<any>();

  constructor() { }

  public setSession(user: any): void {
    this.session.status = true;
    this.session.user.name = user.data.name;
    this.session.user.lastname = user.data.lastname;
    this.session.user.email = user.data.email;
    this.session.user.apiKey = user.data.apiKey;
    this.session.user.apiSecret = user.data.apiSecret;
    this.session.user.mount = user.data.mount;
    this.session.user.token = user.token;
    this.emitChangeInSession();
  }

  public getSession(): Object {
    return this.session
  }

  private emitChangeInSession(): void {
    this.session$.next(this.session);
  }

  getSessionData$(): Observable<any> {
    return this.session$.asObservable();
  }
}
