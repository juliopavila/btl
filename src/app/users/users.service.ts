import { UserSessionService } from 'src/app/user-session.service';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpHeaders, HttpClient } from '@angular/common/http';

//enviroment
import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';




@Injectable({
  providedIn: 'root'
})
export class UsersService {

  //Static data 
  baseUrl: string = environment.baseUrl
  userData: any;
  httpHeaders: any;

  constructor(
    public http: HttpClient,
    private router: Router,
    private userSessionService: UserSessionService
  ) {
    this.userData = this.userSessionService.getSession();
    this.httpHeaders = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + this.userData.user.token
      })
    }
  }

  /**
   * Method for send petition to change email
   * @param body any
   * @returns Observable
   */
  changeEmail(body: any): Observable<any> {
    const url = `${this.baseUrl}/session/profile`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }

  /**
   * Method for send petition to change invest mount
   * @param body any
   * @returns Observable
   */
  changeMount(body: any): Observable<any> {
    const url = `${this.baseUrl}/session/mount`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }

  /**
   * Method for send petition to change user password
   * @param body any
   * @returns Observable
   */
  changePassword(body: any): Observable<any> {
    const url = `${this.baseUrl}/session/password`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }

  /**
   * Method for send petition to change API Key
   * @param body any
   * @returns Observable
   */
  changeApiKey(body: any): Observable<any> {
    const url = `${this.baseUrl}/session/apiKey`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }

  /**
   * Method for send petition to change API Secret
   * @param body any
   * @returns Observable
   */
  changeApiSecret(body: any): Observable<any> {
    const url = `${this.baseUrl}/session/apiKey`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }

  /**
   * Method for close user session.
   * @returns Observable
   */
  logout(): Observable<any> {
    const url = `${this.baseUrl}/session/logout`;
    return this.http.get(url, this.httpHeaders).pipe();
  }

  /**
  * Method for redirect to another page.
  * @param url String
  * @returns void.
  */
  redirect(url: string): void {
    this.router.navigate([`${url}`]);
  }
}
