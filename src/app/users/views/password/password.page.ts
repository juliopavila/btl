import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from '../../users.service';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.page.html',
  styleUrls: ['./password.page.scss'],
})
export class PasswordPage {

  passForm: FormGroup;
  actualEmail: string;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public userSessionService: UserSessionService,
    private service: UsersService
  ) {
    this.passForm = this.formBuilder.group({
      oldPassword: new FormControl(null, [Validators.required]),
      newPassword: new FormControl(null, [Validators.required]),
      confPass: new FormControl(null, [Validators.required]),
    },
      this.passwordMatchValidator)
  }

  /**
  * Method for validate password
  */
  passwordMatchValidator = (passForm: FormGroup) => {
    return passForm.get("newPassword").value === passForm.get("confPass").value ? null : { mismatch: true };
  };

  /**
  * Method for call update password service.
  * @returns void.
  */
  updatePass(): void {
    this.loading();
    this.service.changePassword(this.passForm.value)
      .subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 200) {
          this.service.logout();
          this.alert('Confirmación', 'Se ha cambiado la contraseña satisfactoriamente.', 200);
        }
        else {
          this.alert('Error', 'Ha ocurrido un error, intente de nuevo.', 404);
        }
      })
  }

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }

  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string, status: number): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if (status === 200) this.goto('/');
          }
        }
      ]
    });
    await alert.present();
  }

}
