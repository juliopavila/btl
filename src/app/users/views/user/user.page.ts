import { LoadingController, AlertController } from '@ionic/angular';

import { Component } from '@angular/core';
import { UsersService } from '../../users.service';

@Component({
  selector: 'app-user',
  templateUrl: 'user.page.html',
  styleUrls: ['user.page.scss']
})
export class UserPage {

  constructor(
    private service: UsersService,
    public loadingController: LoadingController,
    public alertController: AlertController
  ) { }

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }

  logout(): void {
    this.loading();
    this.service.logout()
      .subscribe(res => {
        this.loadingController.dismiss();
        if(res.status === 200) this.goto('/');
        else this.alert('Error', 'Ha ocurrido un error, intente de nuevo.');
      })
  }

    /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
          }
        }
      ]
    });
    await alert.present();
  }
}
