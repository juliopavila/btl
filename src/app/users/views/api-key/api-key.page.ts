import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from '../../users.service';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-api-key',
  templateUrl: './api-key.page.html',
  styleUrls: ['./api-key.page.scss'],
})
export class ApiKeyPage implements OnInit {

  apiKeyForm: FormGroup;
  userData: any;
  actualApiKey: String;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public userSessionService: UserSessionService,
    private service: UsersService
  ) {
    this.apiKeyForm = this.formBuilder.group({
      apiKey: new FormControl(null, [Validators.required]),
    })
  }

  ngOnInit() {
    this.userData = this.userSessionService.getSession();
    this.actualApiKey = this.userData.user.apiKey;
  }

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }


  /**
  * Method for call update apiKey service.
  * @returns void.
  */
  updateKey(): void {
    this.loading();
    this.service.changeApiKey(this.apiKeyForm.value)
      .subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 200) {
          let aux = this.apiKeyForm.value;
          this.alert('Confirmación', 'Se ha actualizado la api key satisfactoriamente',200);
          this.userSessionService.session.user.apiKey = aux.apiKey;
        }
        else {
          this.alert('Error', 'Ha ocurrido un error, intente de nuevo.',404);
        }
      })
  }


  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string, status: number): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if(status === 200) this.goto('/tabs/users/');
          }
        }
      ]
    });
    await alert.present();
  }
}
