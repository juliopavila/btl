import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from '../../users.service';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-api-secret',
  templateUrl: './api-secret.page.html',
  styleUrls: ['./api-secret.page.scss'],
})
export class ApiSecretPage implements OnInit {

  apiSecretForm: FormGroup;
  userData: any;
  actualApiSecret: String;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public userSessionService: UserSessionService,
    private service: UsersService
  ) {
    this.apiSecretForm = this.formBuilder.group({
      apiSecret: new FormControl(null, [Validators.required]),
    })
  }

  ngOnInit() {
    this.userData = this.userSessionService.getSession();
    this.actualApiSecret = this.userData.user.apiSecret;
  }

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }


  /**
  * Method for call update apiKey service.
  * @returns void.
  */
  updateSecret(): void {
    this.loading();
    this.service.changeApiSecret(this.apiSecretForm.value)
      .subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 200) {
          let aux = this.apiSecretForm.value;
          this.alert('Confirmación', 'Se ha actualizado la api secret satisfactoriamente', 200);
          this.userSessionService.session.user.apiSecret = aux.apiSecret;
        }
        else {
          this.alert('Error', 'Ha ocurrido un error, intente de nuevo.', 404);
        }
      })
  }


  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string, status: number): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if (status === 200) this.goto('/tabs/users/');
          }
        }
      ]
    });
    await alert.present();
  }

}
