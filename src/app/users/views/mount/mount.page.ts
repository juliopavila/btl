import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from '../../users.service';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-mount',
  templateUrl: './mount.page.html',
  styleUrls: ['./mount.page.scss'],
})
export class MountPage implements OnInit {

  mountForm: FormGroup;
  userData: any;
  actualMount: string;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public userSessionService: UserSessionService,
    private service: UsersService
  ) {
    this.mountForm = this.formBuilder.group({
      mount: new FormControl(null, [Validators.required]),
    });
  }

  ngOnInit() {
    this.userData = this.userSessionService.getSession();
    this.actualMount = this.userData.user.mount;
  }

  /**
  * Method for call redirect service.
  * @param url String
  */
  goto(url: string): void {
    this.service.redirect(url);
  }

  /**
  * Method for call update mount service.
  * @returns void.
  */
  updateMount(): void {
    this.loading();
    this.service.changeMount(this.mountForm.value)
      .subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 200) {
          let aux = this.mountForm.value;
          this.alert('Confirmación', 'Se ha actualizado el monto satisfactoriamente', 200);
          this.userSessionService.session.user.mount = aux.mount;
        }
        else {
          this.alert('Error', 'Ha ocurrido un error, intente de nuevo.', 404);
        }
      })
  }

  /**
 * Method for create spinner.
 * @returns void.
 */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string, status: number): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            if (status === 200) this.goto('/tabs/users/');
          }
        }
      ]
    });
    await alert.present();
  }
}
