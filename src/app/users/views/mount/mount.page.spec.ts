import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MountPage } from './mount.page';

describe('MountPage', () => {
  let component: MountPage;
  let fixture: ComponentFixture<MountPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MountPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MountPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
