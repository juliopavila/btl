import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { UsersService } from '../../users.service';
import { UserSessionService } from 'src/app/user-session.service';

@Component({
  selector: 'app-email',
  templateUrl: './email.page.html',
  styleUrls: ['./email.page.scss'],
})
export class EmailPage implements OnInit {

  emailForm: FormGroup;
  userData: any;
  actualEmail: string;

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    public userSessionService: UserSessionService,
    private service: UsersService
  ) {
    this.emailForm = this.formBuilder.group({
      email: new FormControl(null, [Validators.required, Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]),
    })
  }

  ngOnInit() {
    this.userData = this.userSessionService.getSession();
    this.actualEmail = this.userData.user.email;
  }

  /**
 * Method for call redirect service.
 * @param url String
 */
  goto(url: string): void {
    this.service.redirect(url);
  }

  /**
  * Method for call update email service.
  * @returns void.
  */
  updateEmail(): void {
    this.loading();
    this.service.changeEmail(this.emailForm.value)
      .subscribe(res => {
        this.loadingController.dismiss();
        if (res.status === 200) {
          this.alert('Confirmación', 'Se ha actualizado satisfactoriamente el correo electrónico.');
          this.service.logout();
        }
        else
          this.alert('Error', 'Ha ocurrido un error, intente de nuevo.')
      })
  }

  /**
   * Method for create spinner.
   * @returns void.
   */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }

  /**
   * Method for create alert
   * @param title String
   * @param msg String
   * @returns Promise void
   */
  async alert(title: string, msg: string): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.goto('/');
          }
        }
      ]
    });
    await alert.present();
  }
}
