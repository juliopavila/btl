import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Pages
import { UserPage } from './views/user/user.page';
import { EmailPage } from './views/email/email.page';
import { MountPage } from './views/mount/mount.page';
import { ApiSecretPage } from './views/api-secret/api-secret.page';
import { ApiKeyPage } from './views/api-key/api-key.page';
import { PasswordPage } from './views/password/password.page';
//Service
import { UsersService } from './users.service';


@NgModule({
    declarations: [
        UserPage,
        ApiKeyPage,
        ApiSecretPage,
        MountPage,
        EmailPage,
        PasswordPage
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { 
                path: '', 
                component: UserPage 
            },
            { 
                path: 'key', 
                component: ApiKeyPage 
            },
            { 
                path: 'secret', 
                component: ApiSecretPage 
            },
            { 
                path: 'mount', 
                component: MountPage 
            },
            { 
                path: 'email', 
                component: EmailPage 
            },
            { 
                path: 'password', 
                component: PasswordPage 
            }
        ])
    ],
    providers: [UsersService]
})
export class UsersModule { }
