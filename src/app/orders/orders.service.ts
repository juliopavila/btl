import { environment } from './../../environments/environment';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { UserSessionService } from '../user-session.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  //Static data 
  baseUrl: string = environment.baseUrl
  userData: any;
  httpHeaders: any;
  constructor(
    private router: Router,
    public http: HttpClient,
    private userSessionService: UserSessionService
  ) {
    this.userData = this.userSessionService.getSession();
    this.httpHeaders = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + this.userData.user.token
      })
    }
  }

  /**
  * Method for redirect to another page.
  * @param url String
  * @returns void.
  */
  redirect(url: string): void {
    this.router.navigateByUrl(url);
  }

  /**
   * Method for get user orders
   * @returns Observable
   */
  getOrders(): Observable<any> {
    const url = `${this.baseUrl}/session/orders`;
    return this.http.get(url, this.httpHeaders).pipe();
  }

  /**
   * Method for get user orders
   * @returns Observable
   */
  deleteOrders(body): Observable<any> {
    const url = `${this.baseUrl}/session/orders`;
    return this.http.put(url, body, this.httpHeaders).pipe();
  }
}
