import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//Pages
import { OrderPage } from './views/order/order.page';
import { ManualOrderPage } from './views/manual-order/manual-order.page';
import { HistoryPage } from './views/history/history.page';
import { BuyPage } from './views/buy/buy.page';
import { SellPage } from './views/sell/sell.page';
//Services
import { OrdersService } from './orders.service';

@NgModule({
    declarations: [
        OrderPage,
        ManualOrderPage,
        HistoryPage,
        BuyPage,
        SellPage
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '',
                component: OrderPage
            },
            {
                path: 'manual',
                component: ManualOrderPage
            },
            {
                path: 'history',
                component: HistoryPage
            },
            {
                path: 'buy',
                component: BuyPage
            },
            {
                path: 'sell',
                component: SellPage
            },
        ])
    ],
    providers: [OrdersService]
})
export class OrderModule { }
