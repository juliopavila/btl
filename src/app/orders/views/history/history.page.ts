import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

  buy: any[] = [];
  sell: any[] = [];
  mode: string;
  load = false;

  constructor(private service: OrdersService) {

  }

  ngOnInit() {
    this.getDataOrders();
    //   this.buy = [
    //     {
    //       par: 'BTC/ETH',
    //       symbol: 'ETH',
    //       mount: 0.255
    //     },
    //     {
    //       par: 'BTC/XRP',
    //       symbol: 'XRP',
    //       mount: 255
    //     },
    //     {
    //       par: 'BTC/XRM',
    //       symbol: 'XRM',
    //       mount: 17
    //     },
    //     {
    //       par: 'BTC/EOS',
    //       symbol: 'EOS',
    //       mount: 27
    //     },
    //   ];
    //   this.sell = [
    //     {
    //       par: 'BTC/ETH',
    //       symbol: 'BTC',
    //       mount: 0.0026
    //     },
    //     {
    //       par: 'BTC/XRP',
    //       symbol: 'BTC',
    //       mount: 0.01536
    //     },
    //     {
    //       par: 'BTC/XRM',
    //       symbol: 'BTC',
    //       mount: 0.0045
    //     },
    //     {
    //       par: 'BTC/EOS',
    //       symbol: 'BTC',
    //       mount: 0.0436
    //     },
    //   ]
  }

  changeMode(event: any): void {
    this.mode = event.detail.value;
  }

  getDataOrders() {
    this.buy = [];
    this.sell = [];
    this.service.getOrders()
      .subscribe((res: any) => {
        console.log(res);
        this.buy = res.orders.shoppingList;
        this.sell = res.orders.salesList;
      })
      console.log(this.buy);
    this.load = true;
    // if (this.load === true) setTimeout(() => {
    //   this.getDataOrders();
    // }, 10000);
  }

}
