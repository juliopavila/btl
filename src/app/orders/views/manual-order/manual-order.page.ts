import { Component } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { LoadingController, AlertController } from '@ionic/angular';
import { OrdersService } from '../../orders.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-manual-order',
  templateUrl: './manual-order.page.html',
  styleUrls: ['./manual-order.page.scss'],
})
export class ManualOrderPage {

  manualForm: FormGroup;
  type: string = 'buy';

  constructor(
    public formBuilder: FormBuilder,
    public loadingController: LoadingController,
    public alertController: AlertController,
    private service: OrdersService,
    private toastController: ToastController
  ) {
    this.manualForm = this.formBuilder.group({
      par: new FormControl(null, [Validators.required, Validators.pattern(/^[A-Z]{3}\/[A-Z]{3}/)]),
      price: new FormControl(null, [Validators.required]),
      mount: new FormControl(null, [Validators.required]),
      total: new FormControl(null, [Validators.required]),
    })
  }

  /**
  * Method for call create manual order service.
  * @returns void.
  */
  async manualOrder(): Promise<void> {
    let body = {
      par: this.manualForm.value.par,
      price: this.manualForm.value.price,
      mount: this.manualForm.value.mount,
      total: this.manualForm.value.total,
      type: this.type
    }
    console.log(body);

    const toast = await this.toastController.create({
      message: 'Compruebe su conexión a internet',
      duration: 2000,
      color:'primary'
    });
    toast.present();

  }

  /**
   * Method for change type
   * @param event any
   * @returns void;
   */
  typeOrder(event: any): void {
    this.type = event.detail.value;
  }
}
