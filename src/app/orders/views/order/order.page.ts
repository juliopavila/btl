import { Component } from '@angular/core';
import { OrdersService } from '../../orders.service';

@Component({
  selector: 'app-order',
  templateUrl: 'order.page.html',
  styleUrls: ['order.page.scss']
})
export class OrderPage {

  constructor(private service: OrdersService) {}

  /**
   * Method for call redirect service.
   * @param url String
   * @returns void.
   */
  goto(url: string): void {
    this.service.redirect(url);
  }
}
