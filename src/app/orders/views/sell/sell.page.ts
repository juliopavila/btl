import { AlertController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders.service';

@Component({
  selector: 'app-sell',
  templateUrl: './sell.page.html',
  styleUrls: ['./sell.page.scss'],
})
export class SellPage implements OnInit {

  active: any[] = [];
  complete: any[] = [];
  load: boolean = false;

  constructor(
    private service: OrdersService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
    this.getDataOrders();
    // this.active = [
    //   {
    //     id: 1,
    //     par: 'BTC/ETH',
    //     symbol: 'ETH',
    //     mount: 0.255
    //   },
    //   {
    //     id: 2,
    //     par: 'BTC/XRP',
    //     symbol: 'XRP',
    //     mount: 255
    //   },
    // ];
    // this.complete = [
    //   {
    //     par: 'BTC/ETH',
    //     symbol: 'ETH',
    //     mount: 0.255
    //   },
    //   {
    //     par: 'BTC/XRP',
    //     symbol: 'XRP',
    //     mount: 255
    //   },
    //   {
    //     par: 'BTC/XRM',
    //     symbol: 'XRM',
    //     mount: 17
    //   },
    //   {
    //     par: 'BTC/EOS',
    //     symbol: 'EOS',
    //     mount: 27
    //   },
    // ]

  }

  getDataOrders() {
    this.complete = [];
    this.active = [];
    this.service.getOrders()
      .subscribe(async (res: any) => {
        console.log(res);
        if (res.orders.salesList.process != true)
          this.complete = res.orders.salesList;
        await console.log(this.complete);
        await console.log(this.active);
      })
    this.load = true;
    // if (this.load === true) setTimeout(() => {
    //   this.getDataOrders();
    // }, 10000);
  }

  deleteOrders(id: number) {
    let body = {
      order_id: id
    }
    this.service.deleteOrders(body)
      .subscribe(res => {
        if (res.status === 200) {
          this.alert('Confirmación', 'Se ha cancelado la orden, satisfactoriamente', 200);
        }
        else this.alert('Error', 'Ha ocurrido un error cancelado la orden.', 400);
      })
  }

  /**
 * Method for create alert
 * @param title String
 * @param msg String
 * @returns Promise void
 */
  async alert(title: string, msg: string, status): Promise<void> {
    const alert = await this.alertController.create({
      header: title,
      message: msg,
      buttons: [
        {
          text: 'Aceptar',
          handler: () => {
            this.complete = [];
            if (status === 200) this.getDataOrders();
          }
        }
      ]
    });
    await alert.present();
  }
}
