import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../orders.service';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.page.html',
  styleUrls: ['./buy.page.scss'],
})
export class BuyPage implements OnInit {

  active: any[] = [];
  complete: any[] = [];
  load = false;

  constructor(
    private service: OrdersService
  ) { }

  ngOnInit() {
    this.getDataOrders();
    // this.active = [
    //   {
    //     id: 1,
    //     par: 'BTC/ETH',
    //     symbol: 'ETH',
    //     mount: 0.255
    //   },
    //   {
    //     id: 2,
    //     par: 'BTC/XRP',
    //     symbol: 'XRP',
    //     mount: 255
    //   },
    // ];
    // this.complete = [
    //   {
    //     par: 'BTC/ETH',
    //     symbol: 'ETH',
    //     mount: 0.255
    //   },
    //   {
    //     par: 'BTC/XRP',
    //     symbol: 'XRP',
    //     mount: 255
    //   },
    //   {
    //     par: 'BTC/XRM',
    //     symbol: 'XRM',
    //     mount: 17
    //   },
    //   {
    //     par: 'BTC/EOS',
    //     symbol: 'EOS',
    //     mount: 27
    //   },
    // ]
  }

  getDataOrders() {
    this.complete = [];
    this.service.getOrders()
      .subscribe((res: any) => {
        console.log(res);
        this.complete = res.orders.shoppingList;
      })
    console.log(this.complete);
    this.load = true;
    // if (this.load === true) setTimeout(() => {
    //   this.getDataOrders();
    // }, 10000);
  }
}
