import { Component, OnInit } from '@angular/core';
import { UserSessionService } from 'src/app/user-session.service';
import { SharedService } from '../../shared.service';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss']
})
export class HomePage implements OnInit {

  userData: any;
  userBalance = {
    balance: undefined,
    statistics: {
      "total": 0,
      "win": 0,
      "lost": 0,
      "process": 0
    },
    investment: {
      "total": 0,
      "win": 0,
      "lose": 0
    }
  };
  price = 10500;
  userBalanceUSD: any;
  load = false;

  constructor(
    public loadingController: LoadingController,
    private userSessionService: UserSessionService,
    private service: SharedService
  ) { }

  /**
   * Method to excute function, when the page load.
   * @returns void.
   */
  ngOnInit(): void {
    this.userData = this.userSessionService.getSession();
    if (this.load === false) this.balance();
  }

  /**
   * Method for call get balance service
   */
  balance(): void {
    this.service.getBalance()
      .subscribe((res: any) => {
        this.userBalance.balance = res.balance + res.investment.win - res.investment.lose;
        this.userBalance.statistics.total = res.statistics.total.toFixed(6);
        this.userBalance.statistics.win = res.statistics.win;
        this.userBalance.statistics.lost = res.statistics.lost;
        this.userBalance.investment.total = res.investment.total.toFixed(6);
        this.userBalance.investment.win = res.investment.win.toFixed(6);
        this.userBalance.investment.lose = res.investment.lose.toFixed(6);
        this.userBalanceUSD = this.userBalance.balance * this.price;
      })
    this.load = true;
    if (this.load === true) setTimeout(() => {
      this.balance();
    }, 10000);
  }

  /**
 * Method for create spinner.
 * @returns void.
 */
  async loading(): Promise<any> {
    const loading = await this.loadingController.create({
      message: 'Espere un momento...',
    });
    return await loading.present();
  }
}
