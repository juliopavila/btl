import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { UserSessionService } from '../user-session.service';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  //Static data 
  baseUrl: string = environment.baseUrl
  userData: any;
  httpHeaders: any;
  constructor(
    public http: HttpClient,
    private userSessionService: UserSessionService
  ) {
    this.userData = this.userSessionService.getSession();
    this.httpHeaders = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        'Authorization': 'Bearer ' + this.userData.user.token
      })
    }
  }

  /**
   * Method for get user balance
   * @returns Observable
   */
  getBalance(): Observable<any> {
    const url = `${this.baseUrl}/session/balance`;
    return this.http.get(url, this.httpHeaders).pipe();
  }
}
