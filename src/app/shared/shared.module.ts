import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
//Pages
import { HomePage } from './views/home/home.page';
import { SharedService } from './shared.service';

@NgModule({
    declarations: [
        HomePage
    ],
    imports: [
        IonicModule,
        CommonModule,
        FormsModule,
        RouterModule.forChild([
            { 
                path: '', 
                component: HomePage 
            }
        ])
    ],
    providers: [SharedService]
})
export class SharedModule { }
